package br.com.neppo;

public class MathUtil {

    /**
     * Dado um conjunto de números inteiros "ints" e um número arbitrário "sum",
     * retorne true caso exista pelo menos um subconjunto de "ints" cuja soma soma dos seus elementos
     * seja igual a "sum"
     *
     * @param ints Conjunto de inteiros
     * @param sum Soma para o subconjunto
     * @return
     * @throws IllegalArgumentException caso o argumento "ints" seja null
     */
    public static boolean subsetSumChecker(int ints[], int sum) throws Exception {

        if (ints == null) {
            throw new IllegalArgumentException("Conjunto nao pode ser nulo!");
        } else {

            int sumAux;
            for (int i = 0 ; i < ints.length; i++){
                sumAux = ints[i];
                for (int j = 0; j< ints.length; j++){
                    if( i != j ){
                        sumAux += ints[j];
                        if(sumAux == sum) {
                            return true;
                        }
                    }
                }
            }

        }
        return false;
    }

}
